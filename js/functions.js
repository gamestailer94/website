(function($){
    var circle_color = '#2196F3'; 
    $('.circle-skill-1').circleProgress({
        value: 0.85,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-2').circleProgress({
        value: 0.70,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-3').circleProgress({
        value: 0.60,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-4').circleProgress({
        value: 0.50,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-5').circleProgress({
        value: 0.80,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-6').circleProgress({
        value: 0.50,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-7').circleProgress({
        value: 0.25,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-8').circleProgress({
        value: 0.10,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-9').circleProgress({
        value: 0.80,
        size: 80,
        fill: circle_color
    });
    $('.circle-skill-10').circleProgress({
        value: 1,
        size: 80,
        fill: circle_color
    });

    function drawProgress(){
        $('.circle-skill-5').circleProgress({
            value: 0.80,
            size: 80,
            fill: circle_color
        });
        $('.circle-skill-6').circleProgress({
            value: 0.50,
            size: 80,
            fill: circle_color
        });
        $('.circle-skill-7').circleProgress({
            value: 0.25,
            size: 80,
            fill: circle_color
        });
        $('.circle-skill-8').circleProgress({
            value: 0.10,
            size: 80,
            fill: circle_color
        });
        $('.circle-skill-9').circleProgress({
            value: 0.80,
            size: 80,
            fill: circle_color
        });
        $('.circle-skill-10').circleProgress({
            value: 1,
            size: 80,
            fill: circle_color
        });
    }

    $('#show-more-skills').on('click', function () {
        $('#more-skills').removeClass('hidden').hide().slideDown();
        $('#show-more-skills').addClass('hidden');
        $('#show-less-skills').removeClass('hidden');
        drawProgress();
    });
    $('#show-less-skills').on('click', function () {
        $('#more-skills').slideUp();
        $('#show-less-skills').addClass('hidden');
        $('#show-more-skills').removeClass('hidden');
    });

    $('#contactBTN').html("<n pynff=\"oga oga-cevznel\" uers=\"znvygb:gvz@cyngmxr.qr\">Pbagnpg Zr</n>".replace(/[a-zA-Z]/g,
        function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));
})(jQuery);