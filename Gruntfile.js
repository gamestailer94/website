module.exports = function (grunt) {
    grunt.initConfig({
        cssmin: {
            dist: {
                files: {
                    'css/app.min.css': ['css/*.css', '!css/*.min.css']
                }
            }
        },
        less:{
            dist: {
                options: {
                    plugins: [
                        new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
                        // new (require('less-plugin-clean-css'))(cleanCssOptions)
                    ]
                },
                files: {
                    'css/style.css': 'css/src/bootstrap.less'
                }
            }
        },
        uglify: {
            dist: {
                options: {
                    sourceMap: true
                },
                files: {
                    'js/app.min.js': ['js/*.js', '!js/*.min.js']
                }
            }
        },
        htmlmin: {
            dist: {
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: {
                    'index.html': 'src/index.html'
                }
            }
        },
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7
                },
                files: {
                    'img/profil.png': 'img/src/profil.png',
                    'img/db.png': 'img/src/db.png',
                    'img/echo_dot.png': 'img/src/echo_dot.png',
                    'img/mail.png': 'img/src/mail.png',
                    'img/quest.png': 'img/src/quest.png',
                    'img/translator.png': 'img/src/translator.png',
                    'img/website.png': 'img/src/website.png'
                }
            }
        },
        watch: {
            options:{
                livereload: true
            },
            less:{
                files:['css/src/*.less'],
                tasks: ['less', 'cssmin']
            },
            js:{
                files: ['js/*.js', '!js/*.min.js'],
                tasks: 'uglify:dist'
            },
            html:{
                files: ['src/index.html'],
                tasks: ['htmlmin']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask('default', ['uglify', 'less', 'cssmin', 'htmlmin']);
};